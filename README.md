# ATLAS Tau Fake Factor Tool -- Data Template Version

**Please note: The tool is not ready for use and is not a current recommendation 
of TauWG!**

The Tau Fake Factor Tool is an effort to harmonize estimation of jets 
reconstructed as tau objects, born from the Fake Tau Task Force (FTTF). The tool 
calculates fake factors (FF) for jets that are misidentified as taus. This is 
achieved by estimating the fraction of fake tau jets initiated by quarks and 
interpolating FF between known FFs associated with Z+jets and multijet processes
for bins of pT and number of prongs. This version of the tool assumes the 
fraction of quark-initiated jets is different in Z+jets and multijet events.
The FF is calculated by a weighted sum of Z+jets and multijet FF where the
weight is given by the fraction of multijet-like events in a given sample.
The nominal method uses MC quark and gluon templates to find the fraction of
quark-initated jets directly and interpolating from Z+jets and multijet FF 
(LINK)

The data template version in this package is meant as backup/closure for the 
nominal method.

# Usage

The Tau Fake Factor Tool (TFFT) exists as a single executable in the Athena 21.2 
framework. The nominal tool requires a specific directory structure to specify 
the region where FF is calculated. In order to accommodate full functionality, 
each directory (each region), should include jet width distributions binned in 
supported pT bins. The tool will parse the directory structure and perform 
interpolation “on-the-fly”.

Note, the FTTF has not provided recommendations for systematics at this time. 
The tool, subsequently, does not provide systematics.

TFFT’s single executable for the data template method (named fakeTauToolData) 
requires one argument with an option flag:

`$ fakeTauToolData \path\to\input\file.root debugFlag = false`

The **debugFlag** is an option argument (set to “false” by default). When set to 
“true”, provides every available histogram used by the tool for every step 
including stacked histograms showing the multijet fraction fit result against 
the Z+jets and multijet data templates. When set to the default, “false”, the 
tool simply provides the multijet fraction fit histograms and interpolated FF.

# Required Input Structure

A specific directory naming structure of inputs to TFFT is required in order to 
match specifics about the region of interest with the appropriate interpolation 
region. An example is to interpolate FF with Z+jets and multijet regions with 
the same BDT working point as the sample of interest.

The naming scheme for directories are:

`SampleName_Prong_BDTWP_WPpassORfail_BDTScoreCut_JvtCut_TriggerPass`

with the following options (* refers to currently available options):

SampleName | Prong | BDTWP | WPpassORfail | BDTScoreCut | JvtCut | TriggerPass
---------- | ----- | ----- | ------------ | ----------- | ------ | -----------
"Arbitrary"	| 1p* | loose | pass* | bdt005* | jvt0* | trig0*
 | 3p* | medium* | fail* | bdt05 | jvt1 | trig1
 | | tight | | | |

**SampleName** is a user-defined identifier which can be any arbitrary 
alphanumeric sequence. **Prong** is the prongness of the sample (one or three 
prong). **BDTWP** is the BDT working point of interest. **WPpassORfail** is 
whether the sample is the BDT pass or fail region. **BDTScoreCut** corresponds 
to a BDT score cut in the sample. **JvtCut** is whether tau objects have a JVT 
cut applied or not. **TriggerPass** is whether the tau objects are 
trigger-matched to a tau trigger in the sample or not. As an example, consider a 
sample of interest which contains one prong tau objects that fail the medium BDT 
working point, with BDT score cut of 0.005 with no JVT cut and are not subjected 
to trigger-matching.

The directory should be named:

`mysample_1p_medium_fail_bdt005_jvt0_trig0`

Each directory should contain histograms of jet width. The histogram naming 
convention defines the pT bins for which the user wishes to interpolate FF.

The naming scheme for histograms is:

`h_ptMinMax_width`

This implies each pT bin should have a jet width histogram. The available bins 
are:

Min | Max
--- | ---
20 | 30
30 | 40
40 | 60
60 | 90
90 | 150
150 | 999

Thus, if one was interested in interpolating FF in pT bins 20-30 and 30-40, the 
following histograms should be included inside the appropriate directory:

`h_pt2030_width`

`h_pt3040_width`

The included histograms must be binned in order for direct comparison to 
provided templates. Jet width histograms should thus have 30 bins from 0-0.3.

* jet width binning:

`{0, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.11, 0.12, 
0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19, 0.2, 0.21, 0.22, 0.23, 0.24, 0.25, 
0.26, 0.27, 0.28, 0.29, 0.3}`

Once the appropriate histograms are created and placed into the appropriate 
directory, the resulting ROOT file can be given directory to TFFT. The tool has 
the capability to handle as many different samples (directories) at once as 
desired.

# Test Input

A sample file which can be used for testing the functionality of the tool is 
available. The sample file is a bin-by-bin 40/60 mixture of multijet/Z+jet 
interpolation regions with pT bins up to 150 GeV.

The file is located at /eos/user/j/jmuse/fakeToolInputs

# Contact

Feel free to contact Joe Muse at joseph.m.muse@ou.edu with any 
questions/concerns/comments about TFFT.

# Documentation

Internal note:

Twiki: 