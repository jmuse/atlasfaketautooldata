#ifndef fakeTauToolData_IfakeFactorDataTemplate_H
#define fakeTauToolData_IfakeFactorDataTemplate_H

#include "AsgTools/IAsgTool.h"
#include <vector>
#include <string>
#include <TH1.h>
#include <TTree.h>

using namespace std;

namespace fakeTauToolData {
    
    class IfakeFactorDataTemplate : public virtual asg::IAsgTool
    {
    
        ASG_TOOL_INTERFACE( fakeTauToolData::IfakeFactorDataTemplate )

        
        public:
            void getFakeFactorsData(const char*, int debugFlag = 0);
            
        private:
            std::vector<TH1F*> *getHistoData(double*, int&, std::vector<std::string>*, std::string&, std::string&, const char*, int debugFlag = 0);
            std::vector<float>* doFitData(TH1F*, TH1F*, TH1F*, std::string&, int debugFlag = 0);            
            std::vector<std::vector<float>> *getMjetFractionsData(std::vector<TH1F*>*, std::vector<TH1F*>*, std::vector<TH1F*>*, double*, int&, std::string&, int debugFlag = 0);
            void interpolateFFData(std::vector<TH1F*>*, std::vector<TH1F*>*, std::vector<std::vector<float>>*, double*, int&, std::string&, int debugFlag = 0);
            std::vector<std::string>* getDirectoriesData(const char*);
            std::vector<std::string>* getCutsData(std::string&);
            std::vector<int> getBinningData(std::string&, const char*);
    };
}

#endif
