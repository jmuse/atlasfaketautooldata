///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
///////         //// ///         //////////////////////////////////////////////////////////            
//////////  / /////// /////  //////////////////////////////////////////////////////////////
//////////  ////       ////  //////////////////////////////////////////////////////////////
////// ///  ///////// /////  /// //////////////////////////////////////////////////////////     
//////      //////// //////      //////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
//                                   //////////////////////////////////////////////////////
//    ATLAS Fake Tau Tool            //////////////////////////////////////////////////////
//    Date: 01/10/2019               //////////////////////////////////////////////////////
//    Author: Joe Muse               //////////////////////////////////////////////////////
//    Email: joseph.m.muse@ou.edu    //////////////////////////////////////////////////////
//                                   //////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////  

#include <fakeTauToolData/fakeFactorDataTemplate.h>
#include <TObjArray.h>
#include <TFractionFitter.h>
#include <Fit/ParameterSettings.h>
#include <Fit/Fitter.h>
#include <TFile.h>
#include <TH1.h>
#include <array>
#include <TBranch.h>
#include <vector>
#include <TRandom3.h>
#include <TChain.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <string>
#include <iostream>
#include <AsgTools/MessageCheck.h>
#include "RooRealVar.h"
#include "RooConstVar.h"
#include "RooGaussian.h"
#include "RooArgusBG.h"
#include "RooAddPdf.h"
#include "RooDataSet.h"
#include "RooPlot.h"
#include <RooStats/HistFactory/Measurement.h>
#include <RooStats/HistFactory/Channel.h>
#include <RooStats/HistFactory/Sample.h>
#include <RooStats/HistFactory/MakeModelAndMeasurementsFast.h>
#include <RooStats/ModelConfig.h>
#include <RooWorkspace.h>
#include <RooMinimizer.h>
#include "AtlasUtils.C"
#include "AtlasStyle.C"
#include "AtlasLabels.C"
#include "fakeTauToolData/AtlasUtils.h"
#include "fakeTauToolData/AtlasStyle.h"
#include "fakeTauToolData/AtlasLabels.h"

//using namespace RooFit;

namespace fakeTauToolData{
    
    fakeFactorDataTemplate::fakeFactorDataTemplate (const std::string& name): asg::AsgTool( name ){
        
        TFile *outFile = new TFile("fakeTauData.root", "RECREATE");
        outFile->Close();

    }

    std::vector<TH1F*>* fakeFactorDataTemplate::getHistoData(double *bins, int &numBins, std::vector<std::string> *inCuts, std::string &variable, std::string &region, const char* inFileName, int debugFlag){

        // get pT or width histo for samples used in interpolation process (i.e. zmumu, jetjet, or your sample of interest)

        std::vector<TH1F*> outHisto;
        std::vector<TH1F*> *cloneOutHisto = new std::vector<TH1F*>;

        TFile inFile(inFileName);
        
        for(int i = 0; i < numBins; i++){
            maxBin = bins[i + 1];
            minBin = bins[i];
            maxInt = maxBin;
            minInt = minBin;
            maxStr = std::to_string(maxInt);
            minStr = std::to_string(minInt);
            histoOIStr =  region + "_" + inCuts->at(1) + "_" + inCuts->at(2) + "_" + inCuts->at(3) + "_" + inCuts->at(4) + "_" + inCuts->at(5) + "_" + inCuts->at(6) + "/h_pt" + minStr + maxStr + "_" + variable;
            titleStr = region + "_" + inCuts->at(1) + "_" + minStr + maxStr + "_" + variable;

            TH1F *tempHisto = (TH1F*)inFile.Get(histoOIStr.c_str());
            TH1F *clone = (TH1F*)tempHisto->Clone("temp");

            clone->SetName(titleStr.c_str());
            clone->SetTitle(titleStr.c_str());

            outHisto.push_back(clone);
        }

        TFile outFile("fakeTauData.root", "UPDATE");

        for(int i = 0; i < outHisto.size(); i++){
            TH1F *clone = (TH1F*)outHisto.at(i)->Clone();
            clone->SetDirectory(0);
            cloneOutHisto->push_back(clone);
            if(debugFlag == 1){
                outHisto.at(i)->Write();
            }
        }

        outFile.Close();
        inFile.Close();

        return cloneOutHisto;

    }

    std::vector<float>* fakeFactorDataTemplate::doFitData(TH1F *inSample, TH1F *inZmumu, TH1F *inMJet, std::string &titleStr, int debugFlag){
        
        // find the multijet fraction in the sample
        
        inSample->Sumw2();
        inZmumu->Sumw2();
        inMJet->Sumw2();
        
        std::vector<float> *fracOut = new std::vector<float>;
        std::string measurementStr = "test_method_" + titleStr;
        RooStats::HistFactory::Measurement meas(measurementStr.c_str(), measurementStr.c_str());
        meas.SetExportOnly(1);
        meas.SetPOI("fracmjet");
        meas.SetLumi(1.0);
        meas.SetLumiRelErr(0.001);
        meas.AddConstantParam("Lumi");

        double startfraczmumu = 0.5;
        double startfracmjet = 0.5;

        RooStats::HistFactory::Channel chan("channel");
        chan.SetData(inSample);

        inZmumu->Scale(inSample->Integral()/inZmumu->Integral());
        inMJet->Scale(inSample->Integral()/inMJet->Integral());

        //set zmumu sample
        RooStats::HistFactory::Sample zmumusamp("zmumusamp");
        zmumusamp.SetHisto(inZmumu);
        zmumusamp.AddNormFactor("fraczmumu", startfraczmumu,0.0001, 1);
        zmumusamp.SetNormalizeByTheory(false);
        chan.AddSample(zmumusamp);

        RooStats::HistFactory::Sample mjetsamp("mjetsamp");
        mjetsamp.SetHisto(inMJet);
        mjetsamp.AddNormFactor("fracmjet", startfracmjet, 0.0001, 1);
        mjetsamp.SetNormalizeByTheory(false);
        chan.AddSample(mjetsamp);

        meas.AddChannel(chan);
        meas.PrintTree();

        RooWorkspace *space = (RooWorkspace*)RooStats::HistFactory::MakeModelAndMeasurementFast(meas);

        std::string fileStr = "_combined_test_method_" + titleStr + "_model.root";
        TFile *newFile = new TFile(fileStr.c_str());
        RooWorkspace *myspace = (RooWorkspace*)newFile->Get("combined");
        RooStats::ModelConfig* datafit = (RooStats::ModelConfig*)myspace->obj("ModelConfig");
        RooDataSet* data = (RooDataSet*)myspace->data("obsData");
        RooArgSet params(*datafit->GetParametersOfInterest());
                                                                                                                           
        RooArgSet* constrainedParams = datafit->GetPdf()->getParameters(*data);
        RooFit::Constrain(*constrainedParams);
        const RooArgSet* glbObs = datafit->GetGlobalObservables();
        RooAbsReal * nll = datafit->GetPdf()->createNLL(*data, RooFit::Constrain(*constrainedParams), RooFit::GlobalObservables(*glbObs), RooFit::Offset(1) );

        RooMinimizer minim(*nll);
        int status = -99;
        status = minim.minimize("Minuit");
     
        RooFitResult *r = (RooFitResult*)minim.save();

        RooRealVar *firstPOI = (RooRealVar*)datafit->GetParametersOfInterest()->first();

        const RooAbsRealLValue *width = (const RooAbsRealLValue*)datafit->GetObservables()->first();
        
        fracOut->push_back(firstPOI->getValV());
        fracOut->push_back(firstPOI->getError());

        TH1 *modelhisto = (TH1*)datafit->GetPdf()->createHistogram("fithisto",*width);
        TH1F *outHisto = new TH1F(titleStr.c_str(),titleStr.c_str(), 30,0,0.3);
        modelhisto->Scale(inSample->Integral()/modelhisto->Integral());
        outHisto->Add(modelhisto);

        TFile outFile("fakeTauData.root", "UPDATE");
        outHisto->Write();
        outFile.Close();
    
        const char* rm = "rm _results.table";
        
        system(rm);
        
        if(debugFlag == 1){
            
            SetAtlasStyle();
            std::string likelihoodStr = "likelihood.eps";
            std::string likelihoodplotStr = titleStr + "_" + likelihoodStr;
            const char* likelihoodplot = likelihoodplotStr.c_str();
    
            std::string fitStr = "fit.eps";
            std::string fitplotStr = titleStr + "_" + fitStr;
            const char* fitplot = fitplotStr.c_str();
            
            TCanvas *c0 = new TCanvas("c0", "c0", 600,600);
            gStyle->SetOptTitle(0);
            //get likelihood function and plot
            RooRealVar *firstPOI = (RooRealVar *)datafit->GetParametersOfInterest()->first();
            RooPlot* pframe = firstPOI->frame();
            nll->plotOn(pframe,RooFit::ShiftToZero()); 
            pframe->SetMinimum(0);
            pframe->SetMaximum(2800);
            pframe->Draw();
            c0->SaveAs(likelihoodplot);

            const RooAbsRealLValue *width = (const RooAbsRealLValue*)datafit->GetObservables()->first();

            inZmumu->Scale(inSample->Integral()/inZmumu->Integral());
            inMJet->Scale(inSample->Integral()/inMJet->Integral());

            TCanvas *c1 = new TCanvas ("c1", "c1", 600,600);
            gStyle->SetOptTitle(0);
            TPad *pad11 = new TPad("pad11", "pad11", 0, 0.27, 1, 1.0);
            gStyle->SetOptTitle(0);
            pad11->Draw();
            pad11->cd();
            pad11->SetBottomMargin(0.013);
            inZmumu->SetMarkerColor(2);
            inZmumu->SetLineColor(2);

            inZmumu->SetMarkerStyle(24);
            inMJet->SetMarkerStyle(25);

            inMJet->SetMarkerColor(3);
            inMJet->SetLineColor(3);
            modelhisto->SetLineColor(kBlue);

            inMJet->Draw("P");
            inSample->Draw("P SAME");
            inZmumu->Draw("P SAME");
            modelhisto->Draw("HIST SAME");

            string alphaStr = to_string(firstPOI->getValV());
            string errorStr = to_string(firstPOI->getError());
            string chiStr = to_string(modelhisto->Chi2Test(inSample, "UU CHI2/NDF P"));
            string labelStr = "#alpha_{MJ} = " + alphaStr;
            string labelStr2 = "#pm" + errorStr;
            string labelStr3 = "#chi^{2}/ndf = " + chiStr;
            const char * label = labelStr.c_str();
            const char * label2 = labelStr2.c_str();
            const char * label3 = labelStr3.c_str();

            inMJet->GetXaxis()->SetTitle("#tau Width");
            inMJet->GetYaxis()->SetTitleOffset(2.25);
            inMJet->GetYaxis()->SetTitle("Events / 0.1");
            inMJet->GetXaxis()->SetTitleSize(0);
            inMJet->GetXaxis()->SetLabelSize(0);
            inMJet->SetMinimum(-0.003);
            myText(       0.65,  0.96, 1, titleStr.c_str());
            ATLASLabel(0.23,0.96,"Internal");
            TLegend *legend1 = new TLegend(0.65,0.70,0.92,0.90);
            myText(       0.67,  0.65, 1, label);
            myText(       0.765,  0.60, 1, label2);
            myText(       0.67,  0.55, 1, label3);
            legend1->AddEntry(inSample, "Sample", "P");
            legend1->AddEntry(inZmumu, "Z#rightarrow#mu#mu", "P");
           // legend1->AddEntry(inZmumu, "W+jets", "P");
            legend1->AddEntry(inMJet, "Multijet", "P");
            legend1->AddEntry(modelhisto, "Fit", "L");
            legend1->SetShadowColor(0);
            legend1->SetLineColor(0);
            legend1->Draw();
            c1->cd();
            TPad *pad12 = new TPad("pad12", "pad12", 0, 0.05, 1, 0.27);
            gStyle->SetOptTitle(0);
            pad12->SetTopMargin(0.02);
            pad12->SetBottomMargin(0.3);
            pad12->Draw();
            pad12->cd();
    
            TH1F *d22 = (TH1F*)inSample->Clone("h1");
            d22->SetLineColor(kBlack);
            d22->SetMinimum(0.85);
            d22->SetMaximum(1.15);
            d22->Sumw2();
            d22->SetStats(0);      // No statistics on lower plot
            d22->Divide(modelhisto);
            d22->SetMarkerStyle(20);
            d22->Draw("p HIST");
    
            d22->GetYaxis()->SetTitle("Mixed/Fit");
            d22->GetYaxis()->SetNdivisions(505);
            d22->GetYaxis()->SetTitleSize(18);
            d22->GetYaxis()->SetTitleFont(43);
            d22->GetYaxis()->SetTitleOffset(1.55);
            d22->GetYaxis()->SetLabelFont(43);
            d22->GetYaxis()->SetLabelSize(18);
            d22->GetXaxis()->SetTitleSize(18);
            d22->GetXaxis()->SetTitleFont(43);
            d22->GetXaxis()->SetTitleOffset(4.);
            d22->GetXaxis()->SetLabelFont(43);
            d22->GetXaxis()->SetLabelSize(18);
            d22->GetXaxis()->SetTitle("Jet Width");
            TLine *lineh1 = new TLine(0,1,0.3,1);
            lineh1->Draw();
            gPad->Update();
            gPad->Modified();

            c1->SaveAs(fitplot);
            
        }
        
        return fracOut;

    }

    std::vector<std::vector<float>>* fakeFactorDataTemplate::getMjetFractionsData(std::vector<TH1F*> *inZmumu, std::vector<TH1F*> *inMultijet, std::vector<TH1F*> *inRegion, double *bins, int &numBins, std::string &prong, int debugFlag){
        
        // do a fit for input samples to Zmumu/Mjet templates

        std::vector<TH1F> fitHistoSample;
        std::vector<float> fracSample;
        std::vector<float> errorSample;
        std::vector<std::vector<float>> *outFractions = new std::vector<std::vector<float>>;

        for(int i = 0; i < numBins; i++){
            
            TH1F *tempMultijet = new TH1F("temp", "temp", 30, 0, 0.3);
            
            for(int mjet = 1; mjet < 31; mjet++){
                tempMultijet->SetBinContent(mjet, inMultijet->at(i)->GetBinContent(mjet));
                tempMultijet->SetBinError(mjet, inMultijet->at(i)->GetBinError(mjet));
            }
            
            minBin = bins[i];
            maxBin = bins[i + 1];
            minInt = minBin;
            maxInt = maxBin;
            minStr = std::to_string(minInt);
            maxStr = std::to_string(maxInt);
            titleSampleStr = minStr + maxStr + "_" + prong + "_sample";

            std::cout << "Running fit for: " << prong << ": " << minStr << "-" << maxStr << std::endl;

            std::vector<float> *inSampleFrac = doFitData(inRegion->at(i), inZmumu->at(i), inMultijet->at(i), titleSampleStr, debugFlag);

            fracSample.push_back(inSampleFrac->at(0));
            errorSample.push_back(inSampleFrac->at(1));

        }

        outFractions->push_back(fracSample);
        outFractions->push_back(errorSample);
        
        return outFractions;
        
    }

    void fakeFactorDataTemplate::interpolateFFData(std::vector<TH1F*>* inZmumuFF, std::vector<TH1F*>* inMultijetFF, std::vector<std::vector<float>> *mjetFractions, double *bins, int &numBins, std::string &prong, int debugFlag){
        
        // find the FF by using a weighted sum of zmumu and mjet templates, where weight is mjet fraction
        
        titleStr = prong + "FF";

        TH1F* FF = new TH1F(titleStr.c_str(), titleStr.c_str(), numBins, bins);

        for(int i = 0; i < numBins; i++){
            zmumuFF = inZmumuFF->at(i)->GetBinContent(1);
            multijetFF = inMultijetFF->at(i)->GetBinContent(1);
            sampleFrac = mjetFractions->at(0).at(i);
            outFF = (sampleFrac*multijetFF) + ((1-sampleFrac)*zmumuFF);
            FF->SetBinContent(i + 1, outFF);
        }

        TFile outFile("fakeTauData.root", "UPDATE");
        FF->Write();
        outFile.Close();

    }
    
    std::vector<std::string>* fakeFactorDataTemplate::getDirectoriesData(const char* inFile){

        std::vector<std::string>* outDirectories = new std::vector<std::string>;

        TFile *file = new TFile(inFile);
        TIter next(gDirectory->GetListOfKeys());
        TKey *key;

        while((key = (TKey*)next())) {
            outDirectories->push_back(std::string(key->GetTitle()));
        }

        return outDirectories;

    }

    std::vector<std::string>* fakeFactorDataTemplate::getCutsData(std::string &inDirectory){

        std::vector<std::string>* outCuts = new std::vector<std::string>;

        directory = inDirectory.c_str();

        std::vector<char> temp;

        for(int i = 0; i < strlen(directory); i++){
            if(directory[i] != '_'){
                temp.push_back(directory[i]);
            }
            if((directory[i] == '_') || (i == (strlen(directory)-1))){
                std::string tempStr;
                for(int j = 0; j < temp.size(); j++){
                    tempStr = tempStr + temp.at(j);
                }
                outCuts->push_back(tempStr);
                temp.clear();
            }
        }

        return outCuts;

    }

    std::vector<int> fakeFactorDataTemplate::getBinningData(std::string &inDirectory, const char* inFile){

        TFile *file = new TFile(inFile);

        std::vector<int> outBins;

        directory = inDirectory.c_str();
        std::vector<char> temp;

        file->cd(directory);
        TKey *iterate_key;
        TIter newnext(gDirectory->GetListOfKeys());

        while((iterate_key = (TKey*)newnext())){
            const char* tempChar =  iterate_key->GetTitle();
            if(tempChar == "h_pt2030_pt") continue;
            if(tempChar == "h_pt2030_FF") continue;
            if(tempChar == "h_pt3040_pt") continue;
            if(tempChar == "h_pt3040_FF") continue;
            if(tempChar == "h_pt4060_pt") continue;
            if(tempChar == "h_pt4060_FF") continue;
            if(tempChar == "h_pt6090_pt") continue;
            if(tempChar == "h_pt6090_FF") continue;
            if(tempChar == "h_pt90150_pt") continue;
            if(tempChar == "h_pt90150_FF") continue;
            if(tempChar == "h_pt150999_pt") continue;
            if(tempChar == "h_pt150999_FF") continue;
            if(tempChar == "h_FF") continue;
       std::cout << tempChar << std::endl;
            for(int i = 4; i < strlen(tempChar); i++){
                if(tempChar[i] == '5') continue;
                if((tempChar[i] == '9') && (tempChar[i-1] == '9') && (tempChar[i-2] == '9')) continue;
                if((tempChar[i] != '_') && (tempChar[i] != '0')){
                    temp.push_back(tempChar[i]);
                }
                if(tempChar[i] == '_') break;
            }
            for(int i = 0; i < temp.size(); i++){
                if((i == (temp.size() - 2)) && (temp.at(i) == '9') && (temp.at(i + 1) == '9')) continue;
                if((i == (temp.size() - 1)) && (temp.at(i) == '9') && (temp.at(i - 1) == '9')){
                    outBins.push_back(999);
                }
                if(temp.at(i) == '1'){
                    outBins.push_back(150);
                }
                else{
                    int val = temp.at(i) - '0';
                    int outValFinal = val * 10;
                    if((outValFinal == 20) || (outValFinal == 30) || (outValFinal == 40) || (outValFinal == 60) || (outValFinal == 90) || (outValFinal == 150) || (outValFinal == 999)){
                        outBins.push_back(outValFinal);
                    }
                }
            }
            temp.clear();
        }
        
        file->Close();

        return outBins;

    }

    void fakeFactorDataTemplate::getFakeFactorsData(const char* SampleFileName, int debugFlag){

        // coordinate functions

        std::cout<< "///////////////////////////////////////" <<std::endl;
        std::cout<< "///////////////////////////////////////" <<std::endl;
        std::cout<< "///////         //// ///         //////" <<std::endl;
        std::cout<< "//////////  / /////// /////  //////////" <<std::endl;
        std::cout<< "//////////  ////       ////  //////////" <<std::endl;
        std::cout<< "////// ///  ///////// /////  /// //////" <<std::endl;     
        std::cout<< "//////      //////// //////      //////" <<std::endl;
        std::cout<< "///////////////////////////////////////" <<std::endl;
        std::cout<< "//                                   //" <<std::endl;
        std::cout<< "//    ATLAS Fake Tau Tool            //" <<std::endl;
        std::cout<< "//    Date: 01/10/2019               //" <<std::endl;
        std::cout<< "//    Author: Joe Muse               //" <<std::endl;
        std::cout<< "//    Email: joseph.m.muse@ou.edu    //" <<std::endl;
        std::cout<< "//                                   //" <<std::endl;
        std::cout<< "///////////////////////////////////////" <<std::endl;  

        Zmumu = "Zmumu";
        Multijet = "Multijet";
        pt = "pt";
        width = "width";
        ff = "FF";
        ZmumuFileName = "/eos/user/j/jmuse/fakeToolInputs/Zmumu_inputs_for_technical_implementation_190323.root";
        MultijetFileName = "/eos/user/j/jmuse/fakeToolInputs/Multijet_inputs_280319.root";

        std::vector<std::string> *directories = getDirectoriesData(SampleFileName);

        for(int i = 0; i < directories->size(); i++){
            std::cout << directories->at(i) << std::endl;
            std::vector<std::string>* cuts = getCutsData(directories->at(i));
            std::string Sample = cuts->at(0);
            std::string prong = cuts->at(1);
            std::vector<int> binning = getBinningData(directories->at(i), SampleFileName);
            sort(binning.begin(), binning.end());
            binning.erase(unique(binning.begin(), binning.end()), binning.end());

            int size = binning.size();
            numBins = size - 1;
            double bins[numBins];
            std::copy(binning.begin(), binning.end(), bins);

            std::cout << "Running: " + prong << std::endl;
  
            std::cout << "Running: zmumuWidthHisto" << std::endl;
            std::vector<TH1F*> *zmumuWidthHisto = getHistoData(bins, numBins, cuts, width, Zmumu, ZmumuFileName, debugFlag);
            
            std::cout << "Running: multijetWidthHisto" << std::endl;
            std::vector<TH1F*> *multijetWidthHisto = getHistoData(bins, numBins, cuts, width, Multijet, MultijetFileName, debugFlag);

            std::cout << "Running: sampleWidthHisto" << std::endl;
            std::vector<TH1F*> *sampleWidthHisto = getHistoData(bins, numBins, cuts, width, Sample, SampleFileName, debugFlag);

            std::cout << "Running: zmumuFFHisto" << std::endl;
            std::vector<TH1F*> *zmumuFFHisto = getHistoData(bins, numBins, cuts, ff, Zmumu, ZmumuFileName, debugFlag);
            
            std::cout << "Running: multijetFFHisto" << std::endl;
            std::vector<TH1F*> *multijetFFHisto = getHistoData(bins, numBins, cuts, ff, Multijet, MultijetFileName, debugFlag);

            std::cout << "Running: mjetFractions" << std::endl;
            std::vector<std::vector<float>> *mjetFractions = getMjetFractionsData(zmumuWidthHisto, multijetWidthHisto, sampleWidthHisto, bins, numBins, prong, debugFlag);

            std::cout << "Running: interpolateFF" << std::endl;
            interpolateFFData(zmumuFFHisto, multijetFFHisto, mjetFractions, bins, numBins, prong, debugFlag);

            delete zmumuWidthHisto;
            delete multijetWidthHisto;
            delete sampleWidthHisto;
            delete zmumuFFHisto;
            delete multijetFFHisto;
            delete mjetFractions;

        }   
        
        std::cout << "All Done!" << std::endl;

    }
}
