// System include(s):
#include <memory>
#include <cstdlib>

// ROOT include(s):
#include <TFile.h>
#include <TError.h>
#include <TString.h>
#include <TH1.h>
#include <vector>
#include <TTree.h>
#include <string>

#include "fakeTauToolData/fakeFactorDataTemplate.h"

int main( int argc, char* argv[] ) {

    fakeTauToolData::fakeFactorDataTemplate fakes("fakeFactor");
    
    const char* inFile = argv[1];
    
    int debugFlag = 0;
    
    if(argc == 2){
        debugFlag = 0;
    }
    
    if(argc == 3){
        debugFlag = atoi(argv[2]);
    }
    
    std::cout << "flag = " << debugFlag << std::endl;
    std::cout << "argc = " << argc << std::endl;
  	
  //	const char* inFile = "/afs/cern.ch/work/j/jmuse/Wjets_test_input2017.root";
  //	const char* inFile = "$FAKETAU/inputs/mixed_samples/hybrid_input.root";
   // const char* inFile = "/eos/user/j/jmuse/fakeToolInputs/Zmumu_inputs_for_technical_implementation_190323.root";
  	
    fakes.getFakeFactorsData(inFile, debugFlag);
    
}
